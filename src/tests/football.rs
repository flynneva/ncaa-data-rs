
use crate::ncaa::football::{
    Football,
    FootballGame,
};
use crate::ncaa::sport::{
    Url,
    BaseQueries,
};
use crate::ncaa::scoreboard::Scoreboard;

use crate::ncaa::game::{
    Game,
    QueryGameInfo,
    QueryBoxscore,
};
use crate::ncaa::football::game_info::FootballGameStatus;
use crate::ncaa::football::boxscore::Boxscore;

#[tokio::test]
async fn test_scoreboard() -> Result<(), reqwest::Error>  {
    let football_fbs = Football::default();

    assert_eq!(football_fbs.url(), "football/fbs");

    let scoreboard: Scoreboard = football_fbs.scoreboard(None).await?;
    assert_eq!(scoreboard.input_md5.is_empty(), false);

    let game = Game{id: scoreboard.get_id_of_game(0)};
    let football_game = FootballGame{game: game, sport: football_fbs};

    let _game_info: FootballGameStatus = football_game.game_info().await?;
    let _boxscore: Boxscore = football_game.boxscore().await?;

    // TODO: if the game hasnt started yet, these are empty
    // assert_eq!(game_info.input_md5.is_empty(), false);
    // assert_eq!(boxscore.input_md5.is_empty(), false);
    Ok(())
}
