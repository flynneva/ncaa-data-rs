
use crate::ncaa::basketball::{
    Basketball,
    BasketballGame,
};
use crate::ncaa::sport::{
    Url,
    BaseQueries,
};
use crate::ncaa::today::Today;
use crate::ncaa::scoreboard::Scoreboard;

use crate::ncaa::game::{
    Game,
    QueryGameInfo,
    QueryBoxscore,
};
use crate::ncaa::basketball::game_info::GameInfo;
use crate::ncaa::basketball::boxscore::Boxscore;

#[tokio::test]
async fn test_scoreboard() -> Result<(), reqwest::Error>  {
    let mens_d1_basketball = Basketball::default();

    assert_eq!(mens_d1_basketball.url(), "basketball-men/d1");

    let today: Today = mens_d1_basketball.today(None).await?;
    assert_eq!(today.input_md5.is_empty(), false);

    let scoreboard: Scoreboard = mens_d1_basketball.scoreboard(None).await?;
    assert_eq!(scoreboard.input_md5.is_empty(), false);

    let game = Game{id: scoreboard.get_id_of_game(0)};
    let basketball_game = BasketballGame{game: game, sport: mens_d1_basketball};

    let _game_info: GameInfo = basketball_game.game_info().await?;
    let _boxscore: Boxscore = basketball_game.boxscore().await?;

    // TODO: if the game hasnt started yet, these are empty
    // assert_eq!(game_info.input_md5.is_empty(), false);
    // assert_eq!(boxscore.input_md5.is_empty(), false);
    Ok(())
}
