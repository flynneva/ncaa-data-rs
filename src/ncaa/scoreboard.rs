
use serde::{Serialize, Deserialize};
use serde_json::Value;
use chrono::Local;

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct Names {
    pub char6: String,
    pub short: String,
    pub seo: String,
    pub full: String
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct Conference {
    #[serde(rename = "conferenceName")]
    pub conference_name: String,
    #[serde(rename = "conferenceSeo")]
    pub conference_seo: String
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct Team {
    pub score: Value,
    pub names: Names,
    pub winner: bool,
    pub seed: Value,
    pub description: String,
    pub rank: String,
    pub conferences: Vec<Conference>,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct GameDetails {
    #[serde(rename = "gameID")]
    pub id: String,
    pub away: Team,
    #[serde(rename = "finalMessage")]
    pub final_message: String,
    #[serde(rename = "bracketRound")]
    pub bracket_round: String,
    pub title: String,
    pub url: String,
    pub network: String,
    pub home: Team,
    #[serde(rename = "liveVideoEnabled")]
    pub live_video_enabled: bool,
    #[serde(rename = "startTime")]
    pub start_time: String,
    #[serde(rename = "startTimeEpoch")]
    pub start_time_epoch: String,
    #[serde(rename = "currentPeriod")]
    pub current_period: String,
    #[serde(rename = "videoState")]
    pub video_state: String,
    #[serde(rename = "bracketRegion")]
    pub bracket_region: String,
    #[serde(rename = "contestClock")]
    pub contest_clock: String
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct Game {
    #[serde(rename = "game")]
    pub details: GameDetails,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct Scoreboard {
    #[serde(rename = "inputMD5Sum")]
    pub input_md5: String,
    pub updated_at: String,
    pub games: Vec<Game>
}

impl std::fmt::Display for Scoreboard {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Number of games: {}", self.games.len())
    }
}

impl Default for Scoreboard {
    fn default() -> Scoreboard {
        Scoreboard {
            input_md5: "".to_string(),
            updated_at: Local::now().to_string(),
            games: Vec::new(),
        }
    }
}

impl Scoreboard {
    pub fn get_id_of_game(&self, index: usize) -> String {
        self.games[index].details.url.rsplit_once("/").unwrap().1.to_string()
    }
}
