use serde::{Serialize, Deserialize};

use crate::ncaa::common::Meta;

#[derive(Debug, Serialize, Deserialize)]
pub struct Summary {
    #[serde(rename = "teamId")]
    team_id: String,
    time: String,
    #[serde(rename = "scoreText")]
    score_text: String,
    #[serde(rename = "driveText")]
    drive_text: Option<String>,
    #[serde(rename = "visitingScore")]
    visiting_score: String,
    #[serde(rename = "homeScore")]
    home_score: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Period {
    pub title: String,
    pub summary: Option<Vec<Summary>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ScoringSummary {
    pub meta: Meta,
    pub periods: Vec<Period>,
}


impl Default for ScoringSummary {
    fn default() -> ScoringSummary {
        ScoringSummary {
            meta: Meta::default(),
            periods: Vec::new(),
        }
    }
}
