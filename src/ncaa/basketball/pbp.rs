use serde::{Serialize, Deserialize};

use crate::ncaa::common::Meta;

#[derive(Debug, Serialize, Deserialize)]
pub struct Play {
    pub score: String,
    pub time: Option<String>,  // Baseball plays do not have a time
    #[serde(rename = "visitorText")]
    pub visitor_text: String,
    #[serde(rename = "homeText")]
    pub home_text: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct FootballPlay {
    #[serde(rename = "scoreText")]
    pub score_text: String,
    #[serde(rename = "driveText")]
    pub drive_text: String,
    #[serde(rename = "teamId")]
    pub team_id: String,
    #[serde(rename = "visitingScore")]
    pub visiting_score: String,
    #[serde(rename = "homeScore")]
    pub home_score: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct FootballPossession {
    #[serde(rename = "teamId")]
    pub team_id: String,
    pub time: String,
    pub plays: Vec<FootballPlay>
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Period {
    // Football
    pub title: Option<String>,
    #[serde(rename = "shortTitle")]
    pub short_title: Option<String>,
    pub possessions: Option<Vec<FootballPossession>>,
    // Pretty much all other sports 
    #[serde(rename = "periodNumber")]
    pub period_number: Option<String>,
    #[serde(rename = "periodDisplay")]
    pub period_display: Option<String>,
    #[serde(rename = "playStats")]
    pub play_stats: Option<Vec<Play>>,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct PlayByPlay {
    #[serde(rename = "inputMD5Sum")]
    pub input_md5: String,
    #[serde(rename = "updatedTimestamp")]
    pub updated_timestamp: String,
    pub meta: Meta,
    pub periods: Vec<Period>,
}