use serde::{Serialize, Deserialize};

use crate::ncaa::common::Meta;

#[derive(Debug, Serialize, Deserialize)]
pub struct PlayerHeader {
    pub position: String,
    #[serde[rename = "minutesPlayed"]]
    pub minutes_played: String,
    #[serde[rename = "fieldGoalsMade"]]
    pub field_goals_made: String,
    #[serde[rename = "threePointsMade"]]
    pub three_points_made: String,
    #[serde[rename = "freeThrowsMade"]]
    pub free_throws_made: String,
    #[serde[rename = "totalRebounds"]]
    pub total_rebounds: String,
    #[serde[rename = "offensiveRebounds"]]
    pub offensive_rebounds: String,
    pub assists: String,
    #[serde[rename = "personalFouls"]]
    pub personal_fouls: String,
    pub steals: String,
    pub turnovers: String,
    #[serde[rename = "blockedShots"]]
    pub blocked_shots: String,
    pub points: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PlayerStats {
    #[serde[rename = "firstName"]]
    pub first_name: String,
    #[serde[rename = "lastName"]]
    pub last_name: String,
    pub position: String,
    #[serde[rename = "minutesPlayed"]]
    pub minutes_played: String,
    #[serde[rename = "fieldGoalsMade"]]
    pub field_goals_made: String,
    #[serde[rename = "threePointsMade"]]
    pub three_points_made: String,
    #[serde[rename = "freeThrowsMade"]]
    pub free_throws_made: String,
    #[serde[rename = "totalRebounds"]]
    pub total_rebounds: String,
    #[serde[rename = "offensiveRebounds"]]
    pub offensive_rebounds: String,
    pub assists: String,
    #[serde[rename = "personalFouls"]]
    pub personal_fouls: String,
    pub steals: String,
    pub turnovers: String,
    #[serde[rename = "blockedShots"]]
    pub blocked_shots: String,
    pub points: String, 
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PlayerTotals {
    #[serde[rename = "fieldGoalsMade"]]
    pub field_goals_made: String,
    #[serde[rename = "threePointsMade"]]
    pub three_points_made: String,
    #[serde[rename = "freeThrowsMade"]]
    pub free_throws_made: String,
    #[serde[rename = "totalRebounds"]]
    pub total_rebounds: String,
    #[serde[rename = "offensiveRebounds"]]
    pub offensive_rebounds: String,
    pub assists: String,
    #[serde[rename = "personalFouls"]]
    pub personal_fouls: String,
    pub steals: String,
    pub turnovers: String,
    #[serde[rename = "blockedShots"]]
    pub blocked_shots: String,
    pub points: String, 
}


#[derive(Debug, Serialize, Deserialize)]
pub struct Team {
    #[serde[rename = "teamId"]]
    pub id: u32,
    #[serde[rename = "playerHeader"]]
    pub player_header: PlayerHeader,
    #[serde[rename = "playerStats"]]
    pub player_stats: Vec<PlayerStats>,
    #[serde[rename = "playerTotals"]]
    pub player_totals: PlayerTotals,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Boxscore {
    #[serde(rename = "inputMD5Sum")]
    pub input_md5: String,
    pub meta: Meta,
    pub teams: Option<Vec<Team>>,
}

impl Default for Boxscore {
    fn default() -> Boxscore {
        Boxscore {
            input_md5: "".to_string(),
            meta: Meta::default(),
            teams: Some(Vec::new()),
        }
    }
}