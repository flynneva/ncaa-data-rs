use serde::{Serialize, Deserialize};
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize)]
pub struct Championship {
    pub division: String,
    pub year: String,
    #[serde(rename = "bracketRound")]
    pub bracket_round: String,
    #[serde(rename = "champLinks")]
    pub champ_links: Vec<Value>,
    #[serde(rename = "championshipId")]
    pub championship_id: String,
    #[serde(rename = "bracketId")]
    pub bracket_id: String,
    pub title: String,
    pub sport: String,
    #[serde(rename = "bracketRegion")]
    pub bracket_region: String
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Tabs {
    pub preview: bool,
    pub boxscore: bool,
    pub recap: bool,
    #[serde(rename = "scoringSummary")]
    pub scoring_summary: bool,
    pub pbp: bool,
    #[serde(rename = "teamStats")]
    pub team_stats: bool
}

#[derive(Debug, Serialize, Deserialize)]
pub struct LineScore {
    pub v: String,
    pub h: String,
    pub per: String
}

#[derive(Debug, Serialize, Deserialize)]
pub struct GameInfoNames {
    #[serde(rename = "6Char")]
    pub char6: String,
    pub short: String,
    pub seo: String,
    pub full: String
}

#[derive(Debug, Serialize, Deserialize)]
pub struct GameInfoTeam {
    pub score: i16,
    pub names: GameInfoNames,
    pub seed: Value,
    pub record: String,
    pub rank: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct GameInfo {
    #[serde(rename = "liveVideos")]
    pub live_videos: Value,
    pub venue: Value,
    #[serde(rename = "inputMD5Sum")]
    pub input_md5: String,
    pub away: GameInfoTeam,
    pub championship: Championship,
    pub tabs: Tabs,
    pub id: String,
    #[serde(rename = "gameLinks")]
    pub game_links: Value,
    pub linescores: Vec<LineScore>,
    pub home: GameInfoTeam,
    pub status: Value,
}

impl Default for GameInfo {
    fn default() -> GameInfo {
        GameInfo {
            live_videos: Value::default(),
            venue: Value::default(),
            input_md5: "".to_string(),
            away: GameInfoTeam::default(),
            championship: Championship::default(),
            tabs: Tabs::default(),
            id: "".to_string(),
            game_links: Value::default(),
            linescores: Vec::new(),
            home: GameInfoTeam::default(),
            status: Value::default(),
        }
    }
}

impl Default for GameInfoNames {
    fn default() -> GameInfoNames {
        GameInfoNames {
            char6: "".to_string(),
            short: "".to_string(),
            seo: "".to_string(),
            full: "".to_string(),
        }
    }
}

impl Default for GameInfoTeam {
    fn default() -> GameInfoTeam {
        GameInfoTeam {
            score: 0,
            names: GameInfoNames::default(),
            seed: Value::default(),
            record: "".to_string(),
            rank: "".to_string(),
        }
    }
}

impl Default for Tabs {
    fn default() -> Tabs {
        Tabs {
            preview: false,
            boxscore: false,
            recap: false,
            scoring_summary: false,
            pbp: false, 
            team_stats: false,
        }
    }
}

impl Default for Championship {
    fn default() -> Championship {
        Championship {
            division: "".to_string(),
            year: "".to_string(),
            bracket_round: "".to_string(),
            champ_links:  Vec::new(),
            championship_id: "".to_string(),
            bracket_id: "".to_string(),
            title: "".to_string(),
            sport: "".to_string(),
            bracket_region: "".to_string(),
        }
    }
}