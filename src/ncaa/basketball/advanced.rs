use serde::{Serialize, Deserialize};

use crate::ncaa::basketball::boxscore::PlayerTotals;


#[derive(Debug, Serialize, Deserialize)]
pub struct TeamAdvancedStats {
    pub field_goals_made: f32,
    pub field_goals_attempted: f32,
    pub free_throws_made: f32,
    pub free_throws_attempted: f32,
    pub threes_made: f32,
    pub threes_attempted: f32,
    pub turnovers: f32,
    pub offensive_rebounds: f32,
    pub defensive_rebounds: f32,
    pub points: f32,
    pub posessions: f32,
    // Effective Field Goal Percentage
    pub efgp: f32,
    // Turnover Percentage
    pub tovp: f32,
    // Offensive Rebound Percentage
    pub orbp: f32,
    // Defensive Rebound Percentage
    pub drbp: f32,
    // Free throw factor
    pub ftf: f32,
    // Points per possession
    pub ppp: f32,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct GameAdvancedStats {
    pub total_posessions: f32,
    pub home: TeamAdvancedStats,
    pub away: TeamAdvancedStats,
}

impl Default for TeamAdvancedStats {
    fn default() -> TeamAdvancedStats {
        TeamAdvancedStats {
            field_goals_made: 0.0,
            field_goals_attempted: 0.0,
            free_throws_made: 0.0,
            free_throws_attempted: 0.0,
            threes_made: 0.0,
            threes_attempted: 0.0,
            turnovers: 0.0,
            offensive_rebounds: 0.0,
            defensive_rebounds: 0.0,
            points: 0.0,
            posessions: 0.0,
            efgp: 0.0,
            tovp: 0.0,
            orbp: 0.0,
            drbp: 0.0,
            ftf: 0.0,
            ppp: 0.0,
        }
    }
}

impl Default for GameAdvancedStats {
    fn default() -> GameAdvancedStats {
        GameAdvancedStats {
            total_posessions: 0.0,
            home: TeamAdvancedStats::default(),
            away: TeamAdvancedStats::default(),
        }
    }
}

pub fn efgp(field_goals_made: f32, threes_made: f32, field_goal_attempts: f32) -> f32 {
    // avoid divide-by-zero errors
    if field_goal_attempts <= 0.0 { return 0.0; }
    ((field_goals_made + (0.5 * threes_made)) / field_goal_attempts) * 100.0
}

pub fn rbp(rebounds: f32, opponent_rebounds: f32) -> f32 {
    if (rebounds + opponent_rebounds) <= 0.0 { return 0.0; }
    (rebounds / (rebounds + opponent_rebounds)) * 100.0
}

pub fn parse_totals(totals: &PlayerTotals) -> TeamAdvancedStats {
    // parse the strings into made/attempts vectors
    let fg_parsed = totals.field_goals_made.rsplit_once("-").unwrap();
    let threes_parsed = totals.three_points_made.rsplit_once("-").unwrap();
    let ft_parsed = totals.free_throws_made.rsplit_once("-").unwrap();
    
    // cast the values to the right types
    let pts: f32 = totals.points.parse().unwrap();
    let fg: f32 = fg_parsed.0.parse().unwrap();
    let threes: f32 = threes_parsed.0.parse().unwrap();
    let threes_attempted: f32 = threes_parsed.1.parse().unwrap();
    let fga: f32 = fg_parsed.1.parse().unwrap();
    let tov: f32 = totals.turnovers.parse().unwrap();
    let ft: f32 = ft_parsed.0.parse().unwrap();
    let fta: f32 = ft_parsed.1.parse().unwrap();
    let orb: f32 = totals.offensive_rebounds.parse().unwrap();
    let trb: f32 = totals.total_rebounds.parse().unwrap();
    let drb: f32 = trb - orb;

    let posessions: f32 = fga - orb + (0.475 * fta) + tov;

    TeamAdvancedStats{
        points: pts,
        field_goals_made: fg,
        field_goals_attempted: fga,
        free_throws_made: ft,
        free_throws_attempted: fta,
        threes_made: threes,
        threes_attempted: threes_attempted,
        turnovers: tov,
        offensive_rebounds: orb,
        defensive_rebounds: drb,
        posessions: posessions,
        efgp: 0.0,
        tovp: 0.0,
        orbp: 0.0,
        drbp: 0.0,
        ftf: 0.0,
        ppp: 0.0,
    }
}

pub fn calc(home_totals: &PlayerTotals, away_totals: &PlayerTotals) -> GameAdvancedStats {
    let mut gas: GameAdvancedStats = GameAdvancedStats::default();

    // parse the home and away totals into usable structures
    gas.home = parse_totals(home_totals);
    gas.away = parse_totals(away_totals);

    gas.total_posessions = (gas.home.posessions + gas.away.posessions) / 2.0;

    // calculate the statistics

    // eFG percentage
    gas.home.efgp = efgp(gas.home.field_goals_made, gas.home.threes_made, gas.home.field_goals_attempted);
    gas.away.efgp = efgp(gas.away.field_goals_made, gas.away.threes_made, gas.away.field_goals_attempted);

    // Turnover percentage
    if gas.total_posessions > 0.0 {
      gas.home.tovp = gas.home.turnovers / gas.total_posessions * 100.0;
      gas.away.tovp = gas.away.turnovers / gas.total_posessions * 100.0;
    }

    // Offensive rebound percentage
    gas.home.orbp = rbp(gas.home.offensive_rebounds, gas.away.defensive_rebounds);
    gas.home.drbp = rbp(gas.home.defensive_rebounds, gas.away.offensive_rebounds);
    gas.away.orbp = rbp(gas.away.offensive_rebounds, gas.home.defensive_rebounds);
    gas.away.drbp = rbp(gas.away.defensive_rebounds, gas.home.offensive_rebounds);

    // Free throw percentage
    gas.home.ftf = (gas.home.free_throws_attempted / gas.home.field_goals_attempted) * 100.0;
    gas.away.ftf = (gas.away.free_throws_attempted / gas.away.field_goals_attempted) * 100.0;

    // Points per possession
    gas.home.ppp = gas.home.points / gas.total_posessions;
    gas.away.ppp = gas.away.points / gas.total_posessions;

    gas
}