
pub mod game_info;
pub mod boxscore;


use crate::ncaa::sport::{
    Get,
    Sport,
    Url,
    BaseQueries,
    SportFromStr,
};
use crate::ncaa::today::Today;
use crate::ncaa::scoreboard::Scoreboard;

use crate::ncaa::game::{
    Game,
    QueryBoxscore,
    QueryGameInfo,
    GameFromStr,
    GameID,
};

use crate::ncaa::football::boxscore::Boxscore;
use crate::ncaa::football::game_info::FootballGameStatus;

use std::fmt;
use std::str::FromStr;

pub const NAME: &str = "football";

// Every sport must have all possible divisions hard-coded
#[derive(Clone, PartialEq)]
pub enum Division {
    FBS,
    FCS,
    D2,
    D3,
}

// Every sport must have all possible variations hard-coded
#[derive(Clone, PartialEq)]
pub enum Variation {
    NONE,
}

impl Division {

    pub fn name(&self) -> String {
        match *self {
            Division::FBS => "fbs".to_string(),
            Division::FCS=> "fcs".to_string(),
            Division::D2 => "d2".to_string(),
            Division::D3 => "d3".to_string(),
        }
    }
}

impl FromStr for Division {

    type Err = ();

    fn from_str(input: &str) -> Result<Division, Self::Err> {
        match input {
            "fbs" => Ok(Division::FBS),
            "fcs" => Ok(Division::FCS),
            "d2" => Ok(Division::D2),
            "d3" => Ok(Division::D3),
            _ => Err(()),
        }
    }
}

impl fmt::Display for Division {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.name())
    }
}

impl Variation {

    pub fn name(&self) -> String {
        match *self {
            Variation::NONE => "football".to_string(),
        }
    }
}

impl fmt::Display for Variation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.name())
    }
}

#[derive(Clone, PartialEq)]
pub struct Football {
    pub base: Sport<Division, Variation>,
}

impl Get for Football {
    fn sport(&self) -> String { self.base.name.clone() }
    fn division(&self) -> String { self.base.division.name() }
    fn variation(&self) -> String { self.base.variation.name() }
}

impl SportFromStr<Football> for Football {
    type Err = ();

    fn from_str(_sport: &str, division: &str, _variation: &str) -> Result<Football, Self::Err> {
        let Ok(division_from_str) =  Division::from_str(division) else { return Err(()) };
        Ok(Self {
            base: Sport {
                name: NAME.to_string().clone(),
                division: division_from_str,
                variation: Variation::NONE,
            }
        })
    }
}

impl Default for Football {
    fn default() -> Self {
        Self {
            base: Sport {
                name: NAME.to_string().clone(),
                division: Division::FBS,
                variation: Variation::NONE,
            }
        }
    }
}

impl Url for Football {
    fn url(&self) -> String {
        format!(
            "{}/{}",
            self.base.name.to_string(),
            self.base.division.name(),
        )
    }
}

impl BaseQueries for Football {

    async fn today(&self, _:  Option<String>) -> Result<Today, reqwest::Error> {
        self.base.today(Some(self.url())).await
    }

    async fn scoreboard(&self, _:  Option<String>) -> Result<Scoreboard, reqwest::Error> {
        self.base.scoreboard(Some(self.url())).await
    }
}

#[derive(Clone, PartialEq)]
pub struct FootballGame {
    pub game: Game,
    pub sport: Football,
}

impl Get for FootballGame {
    fn sport(&self) -> String { self.sport.base.name.clone() }
    fn division(&self) -> String { self.sport.base.division.name() }
    fn variation(&self) -> String { self.sport.base.variation.name() }
}

impl Default for FootballGame {
    fn default() -> Self {
        Self {
            game: Game::default(),
            sport: Football::default(),
        }
    }
}


impl QueryBoxscore<Boxscore> for FootballGame {
    async fn boxscore(&self) -> Result<Boxscore, reqwest::Error> {
        self.game.boxscore().await
    }
}

impl QueryGameInfo<FootballGameStatus> for FootballGame {
    async fn game_info(&self) -> Result<FootballGameStatus, reqwest::Error> {
        self.game.game_info().await
    }
}

impl GameFromStr<FootballGame> for FootballGame {
    type Err = ();

    fn from_str(_sport: &str, division: &str, _variation: &str, id: &str) -> Result<FootballGame, Self::Err> {
        let Ok(division_from_str) =  Division::from_str(division) else { return Err(()) };
        Ok(Self {
            game: Game { id: id.to_string() },
            sport: Football {
                base: Sport {
                    name: NAME.to_string().clone(),
                    division: division_from_str,
                    variation: Variation::NONE,
                }
            }
        })
    }
}

impl GameID for FootballGame {
    fn id(&self) -> String {self.game.id.clone()}
}
