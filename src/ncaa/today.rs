
// casablanca/schedule/football/fbs/2024/schedule-all-conf.json
// casablanca//schedule/basketball-men/d1/2024/12/schedule-all-conf.json


// To get the valid date string
// schedule/basketball-women/d1/today.json

use serde::{Serialize, Deserialize};


#[derive(Debug, Serialize, Deserialize)]
pub struct Today {
    #[serde(rename = "inputMD5Sum")]
    pub input_md5: String,
    pub description: String,
    pub today: String,
}
    
impl std::fmt::Display for Today {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.today)
    }
}

impl Default for Today {
    fn default() -> Today {
        Today {
            input_md5: "".to_string(),
            description: "default".to_string(),
            today: "".to_string(),
        }
    }
}
