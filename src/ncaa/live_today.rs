use serde::{Serialize, Deserialize};
use serde_json::Value;

use chrono::Local;

#[derive(Debug, Serialize, Deserialize)]
pub struct LiveToday {
    #[serde(rename = "inputMD5Sum")]
    pub input_md5: String,
    pub dates: Vec<Value>,
    #[serde(rename = "updatedTimestamp")]
    pub updated_timestamp: String,
}

impl Default for LiveToday {
    fn default() -> LiveToday {
        LiveToday {
            input_md5: "".to_string(),
            dates: Vec::new(),
            updated_timestamp: Local::now().to_string(),
        }
    }
}