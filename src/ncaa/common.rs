use serde::{Serialize, Deserialize};

use serde_json::Value;

#[cfg(not(feature = "proxy"))]
pub const BASE_URL: &str = "https://data.ncaa.com/casablanca";
#[cfg(feature = "proxy")]
pub const BASE_URL: &str = "https://flynnlabs.dev/ncaa_api/casablanca";

#[derive(Debug, Serialize, Deserialize)]
pub struct Team {
    #[serde(rename = "homeTeam")]
    pub home_team: Value,
    pub id: String,
    #[serde(rename = "seoName")]
    pub seo_name: String,
    #[serde(rename = "sixCharAbbr")]
    pub six_char_abbr: String,
    #[serde(rename = "shortName")]
    pub short_name: Option<String>,
    #[serde(rename = "nickName")]
    pub nickname: Option<String>,
    pub color: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Meta {
    pub title: String,
    pub description: Option<String>,
    pub sport: Option<String>,
    pub division: Option<String>,
    pub gametype: Option<String>,
    pub status: Option<String>,
    pub period: Option<String>,
    pub minutes: Option<String>,
    pub seconds: Option<String>,
    pub teams: Vec<Team>
}

impl Default for Team {
    fn default() -> Team {
        Team {
            home_team: Value::default(),
            id: "".to_string(),
            seo_name: "".to_string(),
            six_char_abbr: "".to_string(),
            short_name: Some("".to_string()),
            nickname: Some("".to_string()),
            color: "".to_string(),
        }
    }
}

impl Default for Meta {
    fn default() -> Meta {
        Meta {
            title: "".to_string(),
            description: Some("".to_string()),
            sport: Some("".to_string()),
            division: Some("".to_string()),
            gametype: Some("".to_string()),
            status: Some("".to_string()),
            period: Some("".to_string()),
            minutes: Some("".to_string()),
            seconds: Some("".to_string()),
            teams: Vec::new(),
        }
    }
}