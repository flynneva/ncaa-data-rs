use reqwest::Result;

use serde::de::DeserializeOwned;
use crate::ncaa::common::BASE_URL;


pub async fn query<T: DeserializeOwned + Default>(request_url: String) -> Result<T> {
    println!("{:#?}", request_url);
    let response: reqwest::Response = reqwest::Client::new()
        .get(request_url)
        .send().await?;
    if response.status() != reqwest::StatusCode::OK {
        Ok(T::default())
    } else {
        Ok(response.json().await?)
    }
}

// TODO: add example here in the doc string
pub async fn live_today<Response: DeserializeOwned + Default>() -> Result<Response> {
    let request_url = format!("{}/casablanca/live-today.json", BASE_URL);
    query(request_url).await
}

