use crate::ncaa::query::query;
use crate::ncaa::common::BASE_URL;

use std::fmt;
use std::default::Default;
use serde::de::DeserializeOwned;

// Every game supports
pub enum GameDetail {
    GameInfo,
    ScoringSummary,
    Boxscore,
    Pbp,
}

impl GameDetail {

    fn name(&self) -> String {
        match *self {
            GameDetail::GameInfo => "gameInfo".to_string(),
            GameDetail::ScoringSummary => "scoringSummary".to_string(),
            GameDetail::Boxscore => "boxscore".to_string(),
            GameDetail::Pbp => "pbp".to_string(),
        }
    }
}

impl fmt::Display for GameDetail {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.name())
    }
}

#[derive(Clone, PartialEq)]
pub struct Game {
    pub id: String,
}

impl Default for Game {
    fn default() -> Self {
        Self {
            id: "000000".to_string(),
        }
    }
}

pub trait GameID {
    fn id(&self) -> String;
}

pub trait GameFromStr<T>{
    type Err;
    fn from_str(sport: &str, division: &str, variation: &str, id: &str) -> Result<T, Self::Err>;
}

pub trait Url {
    fn url(&self) -> String;
}

impl Url for Game {
    fn url(&self) -> String {
        format!("{}/game/{}", BASE_URL, self.id)
    }
}


#[allow(async_fn_in_trait)]
pub trait QueryGameInfo<T: Default> {
    async fn game_info(&self) -> Result<T, reqwest::Error>;
}

impl<T: DeserializeOwned + Default> QueryGameInfo<T> for Game {
    async fn game_info(&self) -> Result<T, reqwest::Error> {
        let request_url: String = format!("{}/{}.json", self.url(), GameDetail::GameInfo.name());
        let game_info = query::<T>(request_url);
        Ok(game_info.await?)
    }
}

#[allow(async_fn_in_trait)]
pub trait QueryBoxscore<T: Default> {
    async fn boxscore(&self) -> Result<T, reqwest::Error>;
}

impl<T: DeserializeOwned + Default> QueryBoxscore<T> for Game {
    async fn boxscore(&self) -> Result<T, reqwest::Error> {
        let request_url: String = format!("{}/{}.json", self.url(), GameDetail::Boxscore.name());
        let boxscore = query::<T>(request_url);
        Ok(boxscore.await?)
    }
}
