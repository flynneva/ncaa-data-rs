use crate::ncaa::query::query;
use crate::ncaa::common::BASE_URL;
use crate::ncaa::today::Today;
use crate::ncaa::scoreboard::Scoreboard;

use std::fmt::Display;

#[derive(Clone, PartialEq)]
pub struct Sport<D, V> {
    pub name: String,
    pub division: D,
    pub variation: V,
}

pub trait SportFromStr<T>{
    type Err;
    fn from_str(sport: &str, division: &str, variation: &str) -> Result<T, Self::Err>;
}

pub trait Get {
    fn sport(&self) -> String;
    fn division(&self) -> String;
    fn variation(&self) -> String;
}

pub trait Url {
    fn url(&self) -> String;
}

impl<D: Display, V: Display> Url for Sport<D, V> {
    fn url(&self) -> String {
        format!("{}-{}/{}", self.name, self.variation, self.division)
    }
}

#[allow(async_fn_in_trait)]
pub trait BaseQueries {
    async fn today(&self, url: Option<String>) -> Result<Today, reqwest::Error>;
    #[allow(refining_impl_trait)]
    async fn scoreboard(&self, url: Option<String>) -> Result<Scoreboard, reqwest::Error>;
}

impl<D: Display, V: Display> BaseQueries for Sport<D, V> {

    async fn today(&self, url: Option<String>) -> Result<Today, reqwest::Error> {
        let request_url: String = format!("{}/schedule/{}/today.json", BASE_URL, url.unwrap_or(self.url()));
        let today = query::<Today>(request_url);
        Ok(today.await?)
    }

    async fn scoreboard(&self, url: Option<String>) -> Result<Scoreboard, reqwest::Error> {
        let today = self.today(url.clone()).await?;
        let request_url: String = format!("{}/scoreboard/{}/{}/scoreboard.json", BASE_URL, url.unwrap_or(self.url()), today.today);
        let scoreboard = query::<Scoreboard>(request_url);
        Ok(scoreboard.await?)
    }
}
