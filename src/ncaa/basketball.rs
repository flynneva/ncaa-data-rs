
pub mod boxscore;
pub mod game_info;
pub mod pbp;
pub mod preview;
pub mod recap;
pub mod scoring_summary;

pub mod advanced;

use crate::ncaa::sport::{
    Get,
    Sport,
    Url,
    BaseQueries,
    SportFromStr,
};
use crate::ncaa::today::Today;
use crate::ncaa::scoreboard::Scoreboard;

use crate::ncaa::game::{
    Game,
    QueryBoxscore,
    QueryGameInfo,
    GameFromStr,
    GameID,
};

use crate::ncaa::basketball::boxscore::Boxscore;
use crate::ncaa::basketball::game_info::GameInfo;

use std::fmt;
use std::str::FromStr;

pub const NAME: &str = "basketball";

// Every sport must have all possible divisions hard-coded
#[derive(Clone, PartialEq)]
pub enum Division {
    D1,
    D2,
    D3
}

// Every sport must have all possible variations hard-coded
#[derive(Clone, PartialEq)]
pub enum Variation {
    MEN,
    WOMEN,
}

impl Division {

    pub fn name(&self) -> String {
        match *self {
            Division::D1 => "d1".to_string(),
            Division::D2 => "d2".to_string(),
            Division::D3 => "d3".to_string(),
        }
    }
}

impl fmt::Display for Division {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.name())
    }
}

impl FromStr for Division {

    type Err = ();

    fn from_str(input: &str) -> Result<Division, Self::Err> {
        match input {
            "d1" => Ok(Division::D1),
            "d2" => Ok(Division::D2),
            "d3" => Ok(Division::D3),
            _ => Err(()),
        }
    }
}

impl Variation {

    pub fn name(&self) -> String {
        match *self {
            Variation::MEN => "men".to_string(),
            Variation::WOMEN => "women".to_string(),
        }
    }
}

impl FromStr for Variation {

    type Err = ();

    fn from_str(input: &str) -> Result<Variation, Self::Err> {
        match input {
            "men" => Ok(Variation::MEN),
            "women" => Ok(Variation::WOMEN),
            _ => Err(()),
        }
    }
}

impl fmt::Display for Variation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.name())
    }
}

#[derive(Clone, PartialEq)]
pub struct Basketball {
    pub base: Sport<Division, Variation>,
}

impl Get for Basketball {
    fn sport(&self) -> String { self.base.name.clone() }
    fn division(&self) -> String { self.base.division.name() }
    fn variation(&self) -> String { self.base.variation.name() }
}

impl Default for Basketball {
    fn default() -> Self {
        Self {
            base: Sport {
                name: NAME.to_string().clone(),
                division: Division::D1,
                variation: Variation::MEN,
            }
        }
    }
}

impl Url for Basketball {
    fn url(&self) -> String {
        format!(
            "{}-{}/{}",
            self.base.name.to_string(),
            self.base.variation.name(),
            self.base.division.name(),
        )
    }
}

impl FromStr for Basketball {
    type Err = ();

    fn from_str(input: &str) -> Result<Basketball, Self::Err> {
        match input {
            NAME => Ok(Basketball::default()),
            _ => Err(()),
        }
    }
}

impl SportFromStr<Basketball> for Basketball {
    type Err = ();

    fn from_str(_sport: &str, division: &str, variation: &str) -> Result<Basketball, Self::Err> {
        let Ok(division_from_str) =  Division::from_str(division) else { return Err(()) };
        let Ok(variation_from_str) = Variation::from_str(variation) else { return Err(()) };
        Ok(Self {
            base: Sport {
                name: NAME.to_string().clone(),
                division: division_from_str,
                variation: variation_from_str,
            }
        })
    }
}

impl BaseQueries for Basketball {

    async fn today(&self, _: Option<String>) -> Result<Today, reqwest::Error> {
        self.base.today(Some(self.url())).await
    }

    async fn scoreboard(&self, _:  Option<String>) -> Result<Scoreboard, reqwest::Error> {
        self.base.scoreboard(Some(self.url())).await
    }
}

#[derive(Clone, PartialEq)]
pub struct BasketballGame {
    pub game: Game,
    pub sport: Basketball,
}

impl Default for BasketballGame {
    fn default() -> Self {
        Self {
            game: Game::default(),
            sport: Basketball::default(),
        }
    }
}

impl Get for BasketballGame {
    fn sport(&self) -> String { self.sport.base.name.clone() }
    fn division(&self) -> String { self.sport.base.division.name() }
    fn variation(&self) -> String { self.sport.base.variation.name() }
}

impl QueryBoxscore<Boxscore> for BasketballGame {
    async fn boxscore(&self) -> Result<Boxscore, reqwest::Error> {
        self.game.boxscore().await
    }
}

impl QueryGameInfo<GameInfo> for BasketballGame {
    async fn game_info(&self) -> Result<GameInfo, reqwest::Error> {
        self.game.game_info().await
    }
}

impl GameFromStr<BasketballGame> for BasketballGame {
    type Err = ();

    fn from_str(_sport: &str, division: &str, variation: &str, id: &str) -> Result<BasketballGame, Self::Err> {
        let Ok(division_from_str) =  Division::from_str(division) else { return Err(()) };
        let Ok(variation_from_str) = Variation::from_str(variation) else { return Err(()) };
        Ok(Self {
            game: Game { id: id.to_string() },
            sport: Basketball {
                base: Sport {
                    name: NAME.to_string().clone(),
                    division: division_from_str,
                    variation: variation_from_str,
                }
            }
        })
    }
}

impl GameID for BasketballGame {
    fn id(&self) -> String {self.game.id.clone()}
}
