use serde::{Serialize, Deserialize};

// Not used yet 
#[derive(Debug, Serialize, Deserialize)]
pub struct FootballGameStatus {
    pub distance: String,
    #[serde(rename = "finalMessage")]
    pub final_message: String,
    pub possession: String,
    pub yardline: String,
    pub clock: String,
    #[serde(rename = "updatedTimestamp")]
    pub updated_timestamp: String,
    pub down: String,
    pub network: String,
    #[serde(rename = "yardsFromGoal")]
    pub yards_from_goal: String,
    pub winner: String,
    #[serde(rename = "startTime")]
    pub start_time: String,
    #[serde(rename = "startTimeEpoch")]
    pub start_time_epoch: String,
    #[serde(rename = "gameState")]
    pub game_state: String,
    #[serde(rename = "currentPeriod")]
    pub current_period: String
}

impl Default for FootballGameStatus {
    fn default() -> FootballGameStatus {
        FootballGameStatus {
            distance: "".to_string(),
            final_message: "".to_string(),
            possession: "".to_string(),
            yardline: "".to_string(),
            clock: "".to_string(),
            updated_timestamp: "".to_string(),
            down: "".to_string(),
            network: "".to_string(),
            yards_from_goal: "".to_string(),
            winner: "".to_string(),
            start_time: "".to_string(),
            start_time_epoch: "".to_string(),
            game_state: "".to_string(),
            current_period: "".to_string(),
        }
    }
}