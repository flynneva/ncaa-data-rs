use serde::{Serialize, Deserialize};

use crate::ncaa::common::Meta;

#[derive(Debug, Serialize, Deserialize)]
pub struct TableElement {
    #[serde(default)]
    pub class: Option<String>,
    pub display: String
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TableRow {
    pub row: Vec<TableElement>
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Tables {
    pub id: String,
    #[serde(rename = "headerColor")]
    pub header_color: String,
    #[serde(rename = "headerClass")]
    pub header_class: String,
    pub header: Vec<TableElement>,
    pub data: Vec<TableRow>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Boxscore {
    pub meta: Meta,
    pub tables: Option<Vec<Tables>>,
}

impl Default for Boxscore {
    fn default() -> Boxscore {
        Boxscore {
            meta: Meta::default(),
            tables: Some(Vec::new()),
        }
    }
}