
// Base modules
pub mod query;
pub mod common;
pub mod sport;
pub mod game;
// Json data structures common to all sports
pub mod today;
pub mod scoreboard;
pub mod live_today;

// Sport-specific modules
pub mod football;
pub mod basketball;

use crate::ncaa::basketball::NAME as BASKETBALL_NAME;
use crate::ncaa::football::NAME as FOOTBALL_NAME;

use std::str::FromStr;

#[derive(Clone, PartialEq)]
pub enum Sports {
    BASKETBALL,
    FOOTBALL,
}

impl Sports {

    pub fn name(&self) -> String {
        match *self {
            Sports::BASKETBALL => BASKETBALL_NAME.to_string(),
            Sports::FOOTBALL => FOOTBALL_NAME.to_string(),
        }
    }
}

impl FromStr for Sports {
    type Err = ();

    fn from_str(sport: &str) -> Result<Sports, Self::Err> {
        match sport {
            BASKETBALL_NAME => Ok(Sports::BASKETBALL),
            FOOTBALL_NAME => Ok(Sports::FOOTBALL),
            _ => Err(()),
        }
    }
}
