# ncaa-data-rs

Rust crate for fetching and parsing data from data.ncaa.com. Optionally this crate could also be
used to create a postgresql database extension using the same NCAA data structures. This can be
useful to collect and store data to do further processing.

## Getting started

Assuming you already have Rust installed, simply clone and install:

```
git clone git@gitlab.com:flynneva/ncaa-data-rs.git
cd ncaa-data-rs/
cargo install --paths .
```

After installation you should now have the CLI app `ncaa_data_rs` installed:

```
ncaa_data_rs --help
```

Which should provide some useful functions / features for you to use.

To build and test:

```
cd ncaa-data-rs/
cargo build
cargo test
```

## postgresql extension

This repository also provides a postgresql extension to make it super easy to create
a postgresql database. This extension exposes the custom NCAA data types to the postgresql.

To build the postgresql extension:

```
cargo pgx init
cargo pgx new ncaa_data_rs
```

To run the extension, simply specify which postgresql version you want to use:

```
cargo pgx run pg13
```

Read more on `pgx` in [the documentation](https://github.com/tcdi/pgx).
