# syntax=docker/dockerfile:1
FROM timescaledev/rust-pgx:latest

ARG POSTGRESQL_VERSION=14
ARG DEBIAN_FRONTEND=noninteractive

ENV POSTGRESQL_VERSION=${POSTGRESQL_VERSION}

RUN cargo pgx new ncaa_data_rs

# Run the rest of the commands as the ``postgres`` user created by the ``postgres-9.3`` package when it was ``apt-get installed``
USER postgres

# Create a PostgreSQL role named ``docker`` with ``docker`` as the password and
# then create a database `docker` owned by the ``docker`` role.
# Note: here we use ``&&\`` to run commands one after the other - the ``\``
#       allows the RUN command to span multiple lines.
#RUN    /etc/init.d/postgresql start &&\
#    psql --command "CREATE USER docker WITH SUPERUSER PASSWORD 'docker';" &&\
#    createdb -O docker docker

# Adjust PostgreSQL configuration so that remote connections to the
# database are possible.
# RUN echo "host all  all    0.0.0.0/0  md5" >> /etc/postgresql/${POSTGRESQL_VERSION}/main/pg_hba.conf

# And add ``listen_addresses`` to ``/etc/postgresql/${POSTGRESQL_VERSION}/main/postgresql.conf``
# RUN echo "listen_addresses='*'" >> /etc/postgresql/${POSTGRESQL_VERSION}/main/postgresql.conf

# Expose the PostgreSQL port
# EXPOSE 5432

# Add VOLUMEs to allow backup of config, logs and databases
# VOLUME  ["/etc/postgresql", "/var/log/postgresql", "/var/lib/postgresql"]

# Copy ncaa_data_rs to docker env
COPY . /ncaa_data_rs
CMD ["sh", "-c", \
     "cd /ncaa_data_rs && cargo pgx init && cargo pgx package"]